package net.guerlab.spring.searchparams;

import java.lang.annotation.*;

/**
 * 字段
 *
 * @author guer
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface Column {

    /**
     * 字段名称
     *
     * @return 字段名称
     */
    String name() default "";

    /**
     * 是否忽略
     *
     * @return 是否忽略
     */
    boolean ignore() default false;
}
