package net.guerlab.spring.searchparams.mybatisplus;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import net.guerlab.spring.searchparams.OrderByType;
import net.guerlab.spring.searchparams.SearchModelType;

/**
 * 排序类型处理
 *
 * @author guer
 */
public class OrderByHandler extends AbstractMyBatisPlusSearchParamsHandler {

    @Override
    public Class<?> acceptClass() {
        return OrderByType.class;
    }

    @Override
    public void setValue(Object object, String fieldName, String columnName, Object value,
            SearchModelType searchModelType, String customSql) {
        QueryWrapper<?> wrapper = (QueryWrapper<?>) object;
        OrderByType type = (OrderByType) value;

        if (type == OrderByType.DESC) {
            wrapper.orderByDesc(columnName);
        } else {
            wrapper.orderByAsc(columnName);
        }
    }
}
