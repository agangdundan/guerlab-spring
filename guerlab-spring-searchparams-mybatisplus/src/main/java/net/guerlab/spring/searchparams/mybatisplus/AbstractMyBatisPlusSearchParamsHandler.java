package net.guerlab.spring.searchparams.mybatisplus;

import net.guerlab.spring.searchparams.SearchParamsHandler;

/**
 * mybatis plus参数处理
 *
 * @author guer
 */
public abstract class AbstractMyBatisPlusSearchParamsHandler implements SearchParamsHandler {

    /**
     * 获取允许处理的数据类型
     *
     * @return 允许处理的数据类型
     */
    public abstract Class<?> acceptClass();
}
