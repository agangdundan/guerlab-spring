package net.guerlab.spring.searchparams.mybatisplus;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import net.guerlab.spring.searchparams.SearchModelType;
import net.guerlab.spring.searchparams.SearchParamsHandler;

/**
 * 默认处理
 *
 * @author guer
 */
public class DefaultHandler implements SearchParamsHandler {

    @Override
    public void setValue(Object object, String fieldName, String columnName, Object value,
            SearchModelType searchModelType, String customSql) {
        QueryWrapper<?> wrapper = (QueryWrapper<?>) object;
        switch (searchModelType) {
            case GREATER_THAN:
                wrapper.gt(columnName, value);
                break;
            case GREATER_THAN_OR_EQUAL_TO:
                wrapper.ge(columnName, value);
                break;
            case IS_NOT_NULL:
                wrapper.isNotNull(columnName);
                break;
            case IS_NULL:
                wrapper.isNull(columnName);
                break;
            case LESS_THAN:
                wrapper.lt(columnName, value);
                break;
            case LESS_THAN_OR_EQUAL_TO:
                wrapper.le(columnName, value);
                break;
            case NOT_EQUAL_TO:
            case NOT_LIKE:
            case START_NOT_WITH:
            case END_NOT_WITH:
                wrapper.ne(columnName, value);
                break;
            case CUSTOM_SQL:
                if (customSql == null) {
                    break;
                }

                CustomerSqlInfo info = new CustomerSqlInfo(customSql);
                if (info.batch) {
                    wrapper.apply(info.sql.replaceAll(CustomerSqlInfo.BATCH_REG, "{0}"), value);
                } else if (info.matchFlag) {
                    wrapper.apply(info.sql.replaceAll(CustomerSqlInfo.MATCH_REG, "{0}"), value);
                } else {
                    wrapper.apply(info.sql);
                }
                break;
            default:
                wrapper.eq(columnName, value);
        }
    }
}
