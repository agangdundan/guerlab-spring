package net.guerlab.spring.searchparams.mybatisplus;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import net.guerlab.spring.searchparams.SearchModelType;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 集合元素处理
 *
 * @author guer
 */
public class CollectionHandler extends AbstractMyBatisPlusSearchParamsHandler {

    @Override
    public Class<?> acceptClass() {
        return Collection.class;
    }

    private static String buildReplacement(int size) {
        StringBuilder replacementBuilder = new StringBuilder();
        replacementBuilder.append("(");
        for (int i = 0; i < size; i++) {
            if (i != 0) {
                replacementBuilder.append(", ");
            }
            replacementBuilder.append("{");
            replacementBuilder.append(i);
            replacementBuilder.append("}");
        }
        replacementBuilder.append(")");
        return replacementBuilder.toString();
    }

    @SuppressWarnings("unchecked")
    @Override
    public void setValue(Object object, String fieldName, String columnName, Object value,
            SearchModelType searchModelType, String customSql) {
        Collection<Object> collection = (Collection<Object>) value;

        if (collection.isEmpty()) {
            return;
        }

        List<Object> list = collection.stream().filter(Objects::nonNull).collect(Collectors.toList());

        if (list.isEmpty()) {
            return;
        }

        QueryWrapper<?> wrapper = (QueryWrapper<?>) object;
        switch (searchModelType) {
            case NOT_IN:
                wrapper.notIn(columnName, list);
                break;
            case CUSTOM_SQL:
                if (customSql == null) {
                    break;
                }

                CustomerSqlInfo info = new CustomerSqlInfo(customSql);
                String sql = info.sql;
                if (info.matchFlag) {
                    if (info.batch) {
                        while (sql.contains(CustomerSqlInfo.BATCH_FLAG)) {
                            sql = sql.replaceFirst(CustomerSqlInfo.BATCH_REG, buildReplacement(list.size()));
                        }
                    }

                    sql = sql.replaceAll(CustomerSqlInfo.MATCH_REG, "{0}");
                    wrapper.apply(sql, list.toArray());
                } else {
                    wrapper.apply(sql);
                }
                break;
            default:
                wrapper.in(columnName, list);
        }
    }
}
