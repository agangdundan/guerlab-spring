package net.guerlab.spring.searchparams.mybatisplus;

/**
 * 自定义sql信息
 *
 * @author guer
 */
class CustomerSqlInfo {

    public static final String MATCH_FLAG = "?";

    public static final String BATCH_FLAG = "?*";

    public static final String MATCH_REG = "\\?";

    public static final String BATCH_REG = "\\?\\*";

    /**
     * sql片段
     */
    String sql;

    /**
     * 是否包含匹配符
     */
    boolean matchFlag;

    /**
     * 批量模式
     */
    boolean batch;

    /**
     * 通过sql片段构造自定义sql信息
     *
     * @param sql
     *         自定义sql信息
     */
    CustomerSqlInfo(String sql) {
        this.sql = "(" + sql + ")";
        matchFlag = sql.contains(MATCH_FLAG);
        batch = matchFlag && sql.contains(BATCH_FLAG);
    }
}
